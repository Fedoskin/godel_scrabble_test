<?php

return [
    'adminEmail' => 'admin@example.com',
    'siteName' => ucfirst('scrabble club'),
    'defaultDateFormat' => 'Y-m-d H:i:s',
];
