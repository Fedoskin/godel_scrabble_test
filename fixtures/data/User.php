<?php

return [
    [
        'id' => 1,
        'username' => 'test username 1',
        'email' => 'admin@admin.com',
        'password_hash' => Yii::$app->security->generatePasswordHash('111111'),
        'auth_key' => Yii::$app->security->generateRandomString(),
        'confirmed_at' => time(),
        'created_at' => time(),
        'updated_at' => time(),
    ],
    [
        'id' => 2,
        'username' => 'test username 2',
        'email' => 'user@user.com',
        'password_hash' => Yii::$app->security->generatePasswordHash('111111'),
        'auth_key' => Yii::$app->security->generateRandomString(),
        'confirmed_at' => time(),
        'created_at' => time(),
        'updated_at' => time(),
    ],
];
