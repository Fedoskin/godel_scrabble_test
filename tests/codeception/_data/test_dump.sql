-- --------------------------------------------------------
-- Host:                         lawcarta.loc
-- Server version:               5.6.27-0ubuntu0.14.04.1-log - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for godel_scrabble
CREATE DATABASE IF NOT EXISTS `godel_scrabble_unitest` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `godel_scrabble_unitest`;

-- Dumping structure for table godel_scrabble.gst_auth_assignment
DROP TABLE IF EXISTS `gst_auth_assignment`;
CREATE TABLE IF NOT EXISTS `gst_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `gst_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `gst_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table godel_scrabble.gst_auth_assignment: ~0 rows (approximately)
DELETE FROM `gst_auth_assignment`;
/*!40000 ALTER TABLE `gst_auth_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `gst_auth_assignment` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_auth_item
DROP TABLE IF EXISTS `gst_auth_item`;
CREATE TABLE IF NOT EXISTS `gst_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `gst_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `gst_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table godel_scrabble.gst_auth_item: ~0 rows (approximately)
DELETE FROM `gst_auth_item`;
/*!40000 ALTER TABLE `gst_auth_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `gst_auth_item` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_auth_item_child
DROP TABLE IF EXISTS `gst_auth_item_child`;
CREATE TABLE IF NOT EXISTS `gst_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `gst_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `gst_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `gst_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `gst_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table godel_scrabble.gst_auth_item_child: ~0 rows (approximately)
DELETE FROM `gst_auth_item_child`;
/*!40000 ALTER TABLE `gst_auth_item_child` DISABLE KEYS */;
/*!40000 ALTER TABLE `gst_auth_item_child` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_auth_rule
DROP TABLE IF EXISTS `gst_auth_rule`;
CREATE TABLE IF NOT EXISTS `gst_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table godel_scrabble.gst_auth_rule: ~0 rows (approximately)
DELETE FROM `gst_auth_rule`;
/*!40000 ALTER TABLE `gst_auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `gst_auth_rule` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_game
DROP TABLE IF EXISTS `gst_game`;
CREATE TABLE IF NOT EXISTS `gst_game` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `game_date_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- Dumping data for table godel_scrabble.gst_game: ~50 rows (approximately)
DELETE FROM `gst_game`;
/*!40000 ALTER TABLE `gst_game` DISABLE KEYS */;
INSERT INTO `gst_game` (`id`, `name`, `game_date_at`) VALUES
	(1, 'Game in Albania', '2017-05-19 03:41:21'),
	(2, 'Game in American Samoa', '2017-07-27 17:34:17'),
	(3, 'Game in Poland', '2017-07-21 06:20:54'),
	(4, 'Game in Zimbabwe', '2017-03-20 08:53:47'),
	(5, 'Game in Iceland', '2017-04-23 13:00:40'),
	(6, 'Game in Japan', '2017-05-09 01:40:10'),
	(7, 'Game in Romania', '2017-04-06 23:22:41'),
	(8, 'Game in New Zealand', '2017-07-11 17:51:28'),
	(9, 'Game in Trinidad and Tobago', '2017-02-26 10:37:36'),
	(10, 'Game in Oman', '2017-02-14 13:12:32'),
	(11, 'Game in Qatar', '2017-02-22 01:15:01'),
	(12, 'Game in French Guiana', '2017-06-17 08:30:52'),
	(13, 'Game in Bahrain', '2017-02-27 15:00:52'),
	(14, 'Game in Antigua and Barbuda', '2017-06-01 16:28:38'),
	(15, 'Game in Chad', '2017-07-21 05:19:54'),
	(16, 'Game in Norway', '2017-07-15 13:03:05'),
	(17, 'Game in Cyprus', '2017-05-15 09:41:23'),
	(18, 'Game in Virgin Islands (British)', '2017-04-15 07:44:20'),
	(19, 'Game in Thailand', '2017-07-26 17:08:24'),
	(20, 'Game in Burundi', '2017-01-20 03:10:57'),
	(21, 'Game in Belize', '2017-07-04 05:58:26'),
	(22, 'Game in French Polynesia', '2017-06-21 09:48:12'),
	(23, 'Game in Armenia', '2017-05-27 07:20:15'),
	(24, 'Game in Argentina', '2017-01-31 23:48:24'),
	(25, 'Game in Afghanistan', '2017-03-29 07:47:57'),
	(26, 'Game in Lebanon', '2017-03-19 18:01:38'),
	(27, 'Game in Pakistan', '2017-01-31 06:41:34'),
	(28, 'Game in Germany', '2017-04-28 17:17:29'),
	(29, 'Game in Congo, the Democratic Republic of the', '2017-07-13 23:17:50'),
	(30, 'Game in Taiwan, Province of China', '2017-04-18 21:09:05'),
	(31, 'Game in Ecuador', '2017-05-06 05:34:41'),
	(32, 'Game in Bahamas', '2017-05-27 09:15:24'),
	(33, 'Game in Malaysia', '2017-07-14 20:10:23'),
	(34, 'Game in Qatar', '2017-02-18 15:58:07'),
	(35, 'Game in Panama', '2017-03-02 22:35:45'),
	(36, 'Game in Greenland', '2017-07-31 21:20:02'),
	(37, 'Game in Russian Federation', '2017-06-06 07:13:00'),
	(38, 'Game in Kuwait', '2017-03-26 20:43:37'),
	(39, 'Game in British Indian Ocean Territory', '2017-03-18 02:33:27'),
	(40, 'Game in Aruba', '2017-07-26 00:58:09'),
	(41, 'Game in Rwanda', '2017-01-28 22:29:20'),
	(42, 'Game in Virgin Islands (U.S.)', '2017-06-30 20:11:43'),
	(43, 'Game in Burkina Faso', '2017-07-29 08:13:59'),
	(44, 'Game in Kenya', '2017-02-21 19:07:46'),
	(45, 'Game in Holy See (Vatican City State)', '2017-06-26 20:40:07'),
	(46, 'Game in Bulgaria', '2017-03-22 11:04:45'),
	(47, 'Game in Nepal', '2017-01-25 13:48:27'),
	(48, 'Game in Zimbabwe', '2017-02-06 01:20:11'),
	(49, 'Game in Canada', '2017-06-04 07:31:09'),
	(50, 'Game in Chad', '2017-01-02 00:49:31');
/*!40000 ALTER TABLE `gst_game` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_game_score
DROP TABLE IF EXISTS `gst_game_score`;
CREATE TABLE IF NOT EXISTS `gst_game_score` (
  `game_id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `score` tinyint(2) NOT NULL,
  PRIMARY KEY (`game_id`,`user_id`),
  KEY `FK_gst_game_score_gst_user` (`user_id`),
  CONSTRAINT `FK_gst_game_score_gst_game` FOREIGN KEY (`game_id`) REFERENCES `gst_game` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_gst_game_score_gst_user` FOREIGN KEY (`user_id`) REFERENCES `gst_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table godel_scrabble.gst_game_score: ~100 rows (approximately)
DELETE FROM `gst_game_score`;
/*!40000 ALTER TABLE `gst_game_score` DISABLE KEYS */;
INSERT INTO `gst_game_score` (`game_id`, `user_id`, `score`) VALUES
	(1, 4, 59),
	(1, 8, 18),
	(2, 5, 23),
	(2, 8, 12),
	(3, 6, 7),
	(3, 8, 76),
	(4, 1, 30),
	(4, 8, 40),
	(5, 2, 61),
	(5, 8, 80),
	(6, 2, 93),
	(6, 4, 28),
	(7, 1, 27),
	(7, 6, 7),
	(8, 5, 45),
	(8, 6, 90),
	(9, 1, 46),
	(9, 2, 87),
	(10, 5, 43),
	(10, 6, 68),
	(11, 2, 7),
	(11, 4, 99),
	(12, 2, 37),
	(12, 5, 79),
	(13, 2, 14),
	(13, 4, 42),
	(14, 1, 54),
	(14, 8, 84),
	(15, 4, 83),
	(15, 6, 88),
	(16, 1, 25),
	(16, 2, 41),
	(17, 6, 68),
	(17, 8, 89),
	(18, 6, 26),
	(18, 8, 22),
	(19, 1, 17),
	(19, 8, 68),
	(20, 1, 8),
	(20, 8, 100),
	(21, 1, 19),
	(21, 8, 51),
	(22, 2, 24),
	(22, 4, 80),
	(23, 1, 68),
	(23, 2, 46),
	(24, 5, 97),
	(24, 6, 78),
	(25, 4, 89),
	(25, 5, 34),
	(26, 2, 87),
	(26, 8, 27),
	(27, 2, 26),
	(27, 5, 57),
	(28, 1, 96),
	(28, 6, 41),
	(29, 1, 11),
	(29, 6, 18),
	(30, 5, 18),
	(30, 8, 57),
	(31, 6, 58),
	(31, 8, 65),
	(32, 5, 100),
	(32, 6, 49),
	(33, 5, 92),
	(33, 8, 64),
	(34, 4, 82),
	(34, 5, 67),
	(35, 4, 43),
	(35, 6, 22),
	(36, 1, 83),
	(36, 5, 84),
	(37, 4, 11),
	(37, 5, 18),
	(38, 6, 73),
	(38, 8, 79),
	(39, 2, 22),
	(39, 4, 29),
	(40, 5, 16),
	(40, 6, 21),
	(41, 1, 30),
	(41, 6, 39),
	(42, 1, 51),
	(42, 2, 32),
	(43, 6, 21),
	(43, 8, 39),
	(44, 6, 59),
	(44, 8, 28),
	(45, 2, 20),
	(45, 5, 97),
	(46, 2, 86),
	(46, 8, 11),
	(47, 6, 10),
	(47, 8, 12),
	(48, 1, 60),
	(48, 8, 55),
	(49, 4, 89),
	(49, 5, 21),
	(50, 1, 47),
	(50, 6, 89);
/*!40000 ALTER TABLE `gst_game_score` ENABLE KEYS */;

-- Dumping structure for view godel_scrabble.gst_game_statistic
DROP VIEW IF EXISTS `gst_game_statistic`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `gst_game_statistic` (
	`game_id` INT(11) UNSIGNED NOT NULL,
	`highScore` INT(4) NULL,
	`lowScore` INT(4) NULL,
	`avgScore` DECIMAL(8,4) NULL,
	`winner_id` BIGINT(11) NULL,
	`loser_id` BIGINT(11) NULL
) ENGINE=MyISAM;

-- Dumping structure for view godel_scrabble.gst_game_user_statistic
DROP VIEW IF EXISTS `gst_game_user_statistic`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `gst_game_user_statistic` (
	`user_id` INT(11) NOT NULL,
	`username` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`wins` BIGINT(21) NULL,
	`loses` BIGINT(21) NULL,
	`avgnum` DECIMAL(25,4) NULL,
	`avgScore` DECIMAL(7,4) NULL
) ENGINE=MyISAM;

-- Dumping structure for table godel_scrabble.gst_migration_app
DROP TABLE IF EXISTS `gst_migration_app`;
CREATE TABLE IF NOT EXISTS `gst_migration_app` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table godel_scrabble.gst_migration_app: ~0 rows (approximately)
DELETE FROM `gst_migration_app`;
/*!40000 ALTER TABLE `gst_migration_app` DISABLE KEYS */;
INSERT INTO `gst_migration_app` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1501622550);
/*!40000 ALTER TABLE `gst_migration_app` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_migration_rbac
DROP TABLE IF EXISTS `gst_migration_rbac`;
CREATE TABLE IF NOT EXISTS `gst_migration_rbac` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table godel_scrabble.gst_migration_rbac: ~2 rows (approximately)
DELETE FROM `gst_migration_rbac`;
/*!40000 ALTER TABLE `gst_migration_rbac` DISABLE KEYS */;
INSERT INTO `gst_migration_rbac` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1501622543),
	('m140506_102106_rbac_init', 1501622546);
/*!40000 ALTER TABLE `gst_migration_rbac` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_migration_user
DROP TABLE IF EXISTS `gst_migration_user`;
CREATE TABLE IF NOT EXISTS `gst_migration_user` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table godel_scrabble.gst_migration_user: ~13 rows (approximately)
DELETE FROM `gst_migration_user`;
/*!40000 ALTER TABLE `gst_migration_user` DISABLE KEYS */;
INSERT INTO `gst_migration_user` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1501622537),
	('m140209_132017_init', 1501622539),
	('m140403_174025_create_account_table', 1501622539),
	('m140504_113157_update_tables', 1501622539),
	('m140504_130429_create_token_table', 1501622539),
	('m140830_171933_fix_ip_field', 1501622539),
	('m140830_172703_change_account_table_name', 1501622539),
	('m141222_110026_update_ip_field', 1501622539),
	('m141222_135246_alter_username_length', 1501622539),
	('m150614_103145_update_social_account_table', 1501622540),
	('m150623_212711_fix_username_notnull', 1501622540),
	('m151218_234654_add_timezone_to_profile', 1501622540),
	('m160929_103127_add_last_login_at_to_user_table', 1501622540);
/*!40000 ALTER TABLE `gst_migration_user` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_profile
DROP TABLE IF EXISTS `gst_profile`;
CREATE TABLE IF NOT EXISTS `gst_profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `gst_fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `gst_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table godel_scrabble.gst_profile: ~6 rows (approximately)
DELETE FROM `gst_profile`;
/*!40000 ALTER TABLE `gst_profile` DISABLE KEYS */;
INSERT INTO `gst_profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`) VALUES
	(1, 'User1', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'User2', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'User3', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'User4', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'User5', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `gst_profile` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_social_account
DROP TABLE IF EXISTS `gst_social_account`;
CREATE TABLE IF NOT EXISTS `gst_social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gst_account_unique` (`provider`,`client_id`),
  UNIQUE KEY `gst_account_unique_code` (`code`),
  KEY `gst_fk_user_account` (`user_id`),
  CONSTRAINT `gst_fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `gst_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table godel_scrabble.gst_social_account: ~0 rows (approximately)
DELETE FROM `gst_social_account`;
/*!40000 ALTER TABLE `gst_social_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `gst_social_account` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_token
DROP TABLE IF EXISTS `gst_token`;
CREATE TABLE IF NOT EXISTS `gst_token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `gst_token_unique` (`user_id`,`code`,`type`),
  CONSTRAINT `gst_fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `gst_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table godel_scrabble.gst_token: ~0 rows (approximately)
DELETE FROM `gst_token`;
/*!40000 ALTER TABLE `gst_token` DISABLE KEYS */;
INSERT INTO `gst_token` (`user_id`, `code`, `created_at`, `type`) VALUES
	(8, 'hBbd4a5Ra3qTRY7ujGS5UCV2VTV-7gGz', 1501666152, 0);
/*!40000 ALTER TABLE `gst_token` ENABLE KEYS */;

-- Dumping structure for table godel_scrabble.gst_user
DROP TABLE IF EXISTS `gst_user`;
CREATE TABLE IF NOT EXISTS `gst_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gst_user_unique_username` (`username`),
  UNIQUE KEY `gst_user_unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table godel_scrabble.gst_user: ~6 rows (approximately)
DELETE FROM `gst_user`;
/*!40000 ALTER TABLE `gst_user` DISABLE KEYS */;
INSERT INTO `gst_user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`) VALUES
	(1, 'test username 1', 'admin@admin.com', '$2y$13$SgEbAv.OqWX5yYB7ZuaJrOWszSF39DnkGDu2/mu8foYcrbfoBt0xS', 'ozgz1EwFbFM8En_kksl3YESBdEzuKRc0', 1501622821, NULL, NULL, NULL, 1501622821, 1501622821, 0, 1501624358),
	(2, 'test username 2', 'user@user.com', '$2y$13$8hfvjFU2grr74nYNPBKjNO0aFg2cdcUCnTj5rEQm3jQkb63TgnDyu', 'b51eJ1RI-FLffUi5mbsX7T7VZRfvKiqs', 1501622821, NULL, NULL, NULL, 1501622821, 1501622821, 0, 1501761213),
	(4, 'test username 3', 'user3@user.com', '$2y$13$8hfvjFU2grr74nYNPBKjNO0aFg2cdcUCnTj5rEQm3jQkb63TgnDyu', 'b51eJ1RI-FLffUi5mbsX7T7VZRfvKiqs', 1501622821, NULL, NULL, NULL, 1501622821, 1501622821, 0, 1501624469),
	(5, 'test username 4', 'user4@user.com', '$2y$13$8hfvjFU2grr74nYNPBKjNO0aFg2cdcUCnTj5rEQm3jQkb63TgnDyu', 'b51eJ1RI-FLffUi5mbsX7T7VZRfvKiqs', 1501622821, NULL, NULL, NULL, 1501622821, 1501622821, 0, 1501624469),
	(6, 'test username 5', 'user5@user.com', '$2y$13$8hfvjFU2grr74nYNPBKjNO0aFg2cdcUCnTj5rEQm3jQkb63TgnDyu', 'b51eJ1RI-FLffUi5mbsX7T7VZRfvKiqs', 1501622821, NULL, NULL, NULL, 1501622821, 1501622821, 0, 1501624469),
	(8, 'pavel1', 'pavel.televich@gmail.com', '$2y$10$Tn.bzyRF9adVrq2EupGWBOoFZGKGqH70F8jBKhNgb5X2ZlwVDbvQy', 'xkR_QSZG5p4ffu8KlJsauZB_w8dM1vKp', 1501667695, NULL, NULL, '192.168.33.1', 1501666152, 1501667695, 0, 1501757423);
/*!40000 ALTER TABLE `gst_user` ENABLE KEYS */;

-- Dumping structure for view godel_scrabble.gst_game_statistic
DROP VIEW IF EXISTS `gst_game_statistic`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `gst_game_statistic`;
CREATE ALGORITHM=UNDEFINED DEFINER=`developer`@`%` SQL SECURITY DEFINER VIEW `gst_game_statistic` AS select `g`.`id` AS `game_id`,(select max(`gs2`.`score`) from `gst_game_score` `gs2` where (`gs2`.`game_id` = `g`.`id`)) AS `highScore`,(select min(`gs2`.`score`) from `gst_game_score` `gs2` where (`gs2`.`game_id` = `g`.`id`)) AS `lowScore`,(select ((`highScore` + `lowScore`) / 2)) AS `avgScore`,(select `gs`.`user_id` from `gst_game_score` `gs` where ((`gs`.`game_id` = `g`.`id`) and (`gs`.`score` = `highScore`))) AS `winner_id`,(select `gs`.`user_id` from `gst_game_score` `gs` where ((`gs`.`game_id` = `g`.`id`) and (`gs`.`score` = `lowScore`))) AS `loser_id` from `gst_game` `g`;

-- Dumping structure for view godel_scrabble.gst_game_user_statistic
DROP VIEW IF EXISTS `gst_game_user_statistic`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `gst_game_user_statistic`;
CREATE ALGORITHM=UNDEFINED DEFINER=`developer`@`%` SQL SECURITY DEFINER VIEW `gst_game_user_statistic` AS select `u`.`id` AS `user_id`,`u`.`username` AS `username`,(select count(`wgs`.`winner_id`) from `gst_game_statistic` `wgs` where (`wgs`.`winner_id` = `u`.`id`)) AS `wins`,(select count(`lgs`.`loser_id`) from `gst_game_statistic` `lgs` where (`lgs`.`loser_id` = `u`.`id`)) AS `loses`,(select ((`wins` + `loses`) / 2)) AS `avgnum`,(select avg(`gs`.`score`) from `gst_game_score` `gs` where (`gs`.`user_id` = `u`.`id`)) AS `avgScore` from `gst_user` `u`;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
