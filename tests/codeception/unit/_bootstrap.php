<?php
defined('YII_ENV') or define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);

defined('YII_APP_BASE_PATH') or define('YII_APP_BASE_PATH', dirname(dirname(dirname(__DIR__))));

require_once (YII_APP_BASE_PATH .'/vendor/autoload.php');
require_once (YII_APP_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php');

Yii::setAlias('@tests', YII_APP_BASE_PATH . '/tests/');
Yii::setAlias('@data', __DIR__ . DIRECTORY_SEPARATOR . '_data');

$config = require(YII_APP_BASE_PATH . '/config/console.php');

$application = new yii\console\Application( $config );
