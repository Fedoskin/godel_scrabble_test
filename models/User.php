<?php

namespace app\models;

use app\models\query\UserQuery;
use dektrium\user\models\User as BaseUser;
use Yii;
use dektrium\user\helpers\Password;
use yii\web\HttpException;
use yii\web\IdentityInterface;

class User extends BaseUser implements IdentityInterface
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'username'          => \Yii::t('user', 'Username'),
            'email'             => \Yii::t('user', 'Email'),
            'registration_ip'   => \Yii::t('user', 'Registration ip'),
            'unconfirmed_email' => \Yii::t('user', 'New email'),
            'password'          => \Yii::t('user', 'Password'),
            'created_at'        => \Yii::t('user', 'Registration time'),
            'last_login_at'     => \Yii::t('user', 'Last login'),
            'confirmed_at'      => \Yii::t('user', 'Confirmation time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignment()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    public function getToken()
    {
        return $this->hasOne(Token::className(), ['user_id' => 'id']);
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('{{%auth_assignment}}', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserStatistic()
    {
        return $this->hasOne(GameUserStatistic::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts()
    {
        return $this->hasMany(SocialAccount::className(), ['user_id' => 'id']);
    }

    public function setDefaultConfirmedAt()
    {
        $this->confirmed_at = $this->confirmed_at ? 1 : 0;
        return $this;
    }


    public function isAdmin()
    {
        return $this->getRole() == self::ROLE_ADMIN;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        $authRole = $this->hasOne(AuthAssignment::className(), ['user_id' => 'id'])->one();
        if ($authRole) {
            $authRole = $authRole->item_name;
        }
        return $authRole;
    }

    public function assignRoleToUser($role)
    {
        $auth = Yii::$app->authManager;
        $auth->revokeAll($this->id);
        $rbacRole = $auth->getRole($role);

        if ($rbacRole) {
            $auth->assign($rbacRole, $this->id);
        }
        return false;
    }

    public function register()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $transaction = $this->getDb()->beginTransaction();

        try {
            $this->confirmed_at = $this->module->enableConfirmation ? null : time();
            $this->password     = $this->module->enableGeneratingPassword ? Password::generate(8) : $this->password;

            $this->trigger(self::BEFORE_REGISTER);

            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }

            if ($this->module->enableConfirmation) {
                /** @var Token $token */
                $token = \Yii::createObject(['class' => Token::className(), 'type' => Token::TYPE_CONFIRMATION]);
                $token->link('User', $this);
            }

            $this->mailer->sendWelcomeMessage($this, isset($token) ? $token : null);
            $this->trigger(self::AFTER_REGISTER);

            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            return false;
        }
    }

    /**
     * @inheritdoc
     * @throws \yii\web\HttpException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /** @var Token $access */
        $access = Token::findOne(['code' => $token, 'type' => Token::TYPE_ACCESS]);
        if ($access) {
            return $access->user;
        }

        return null;
    }

    /**
     * @param $token
     * @return Token|boolean
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public static function refreshToken($token)
    {
        /** @var Token $token */
        $token = Token::find()->where(['code' => $token, 'type' => Token::TYPE_ACCESS])->one();
        if (!$token || !$token->user) {
            throw new \yii\web\NotFoundHttpException(Yii::t('app', 'User cannot be found.'));
        }
        /** @var User $user */
        $user = $token->user;
        $token->delete();

        return self::createToken($user);
    }

    /**
     * @param User $user
     * @return Token|boolean
     * @throws \yii\base\Exception
     * @throws HttpException
     */
    public static function createToken($user)
    {
        $where = ['user_id' => $user->id, 'type' => Token::TYPE_ACCESS];
        $token = Token::find()
            ->where($where)
            ->one();

        if (!$token) {
            $token_str = Yii::$app->security->generateRandomString();
            $token_check = Token::findOne(['code' => $token_str, 'type' => Token::TYPE_ACCESS]);

            while ($token_check) {
                $token_str = Yii::$app->security->generateRandomString();
                $token_check = Token::findOne(['code' => $token_str, 'type' => Token::TYPE_ACCESS]);
            }

            /** @var Token $token */
            $token = new Token([
                'code' => $token_str,
                'user_id' => $user->id,
                'type' => Token::TYPE_ACCESS,
            ]);

            if (!$token->save()) {
                throw new \yii\web\HttpException(500, $token->getFirstError());
            }
        }

        return $token;
    }

    public function beforeSave($insert)
    {
        if ($this->confirmed_at) {
            $this->confirmed_at = time();
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->setProfile(Yii::createObject(Profile::className()));
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function confirm()
    {
        if (parent::confirm()) {
            self::createToken($this);
            return true;
        }
        return false;
    }

    public function getRelation($name, $throwException = true)
    {
        $name = ucfirst($name);
        return parent::getRelation($name, $throwException); // TODO: Change the autogenerated stub
    }
}
