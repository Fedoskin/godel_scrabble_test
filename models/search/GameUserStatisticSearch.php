<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GameUserStatistic;

/**
 * GameUserStatisticSearch represents the model behind the search form about `app\models\GameUserStatistic`.
 */
class GameUserStatisticSearch extends GameUserStatistic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'wins', 'loses'], 'integer'],
            [['username'], 'safe'],
            [['avgnum', 'avgScore'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GameUserStatistic::find()
            ->where(['>=', 'avgNum', 10])
            ->limit(10)
            ->orderBy('avgScore desc')
            ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'wins' => $this->wins,
            'loses' => $this->loses,
            'avgnum' => $this->avgnum,
            'avgScore' => $this->avgScore,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username]);

        return $dataProvider;
    }
}
