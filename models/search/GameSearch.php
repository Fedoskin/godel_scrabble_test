<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Game;

/**
 * GameSearch represents the model behind the search form about `app\models\Game`.
 */
class GameSearch extends Game
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'game_date_at', 'winner_name', 'loser_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Game::find()
            ->joinWith('gameStatistic as gameStatistic')
            ->joinWith('gameStatistic.winner as winner')
            ->joinWith('gameStatistic.loser as loser')
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes' => [
                'name',
                'game_date_at',
                'winner_name' => [
                    'asc' => [
                        'winner.username' => SORT_ASC,
                    ],
                    'desc' => [
                        'winner.username' => SORT_DESC,
                    ],
                ],
                'loser_name' => [
                    'asc' => [
                        'loser.username' => SORT_ASC,
                    ],
                    'desc' => [
                        'loser.username' => SORT_DESC,
                    ],
                ]
            ]
        ]);

        // grid filtering conditions
        /*$query->andFilterWhere([
            'id' => $this->id,
            'game_date_at' => $this->game_date_at,
        ]);*/

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'game_date_at', $this->game_date_at]);
        $query->andFilterWhere(['like', 'winner.username', $this->winner_name]);
        $query->andFilterWhere(['like', 'loser.username', $this->loser_name]);

        return $dataProvider;
    }
}
