<?php

namespace app\models;

use app\models\query\GameScoreQuery;
use Yii;

/**
 * This is the model class for table "{{%game_score}}".
 *
 * @property integer $game_id
 * @property integer $user_id
 * @property integer $score
 *
 * @property Game $game
 * @property User $user
 */
class GameScore extends \yii\db\ActiveRecord
{
    public static function primaryKey()
    {
        return ['game_id', 'user_id'];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_score}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'user_id', 'score'], 'required'],
            [['game_id', 'user_id', 'score'], 'integer'],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Game::className(), 'targetAttribute' => ['game_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'game_id' => Yii::t('app', 'Game ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'score' => Yii::t('app', 'Score'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatistic()
    {
        return $this->hasOne(GameStatistic::className(), ['game_id' => 'game_id']);
    }

    /**
     * @inheritdoc
     * @return GameScoreQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameScoreQuery(get_called_class());
    }
}
