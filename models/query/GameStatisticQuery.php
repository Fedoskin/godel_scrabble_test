<?php

namespace app\models\query;

use app\models\GameStatistic;

/**
 * This is the ActiveQuery class for [[GameStatistic]].
 *
 * @see GameStatistic
 */
class GameStatisticQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return GameStatistic[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return GameStatistic|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
