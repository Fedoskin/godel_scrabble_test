<?php

namespace app\models\query;

use app\models\GameScore;

/**
 * This is the ActiveQuery class for [[GameScore]].
 *
 * @see GameScore
 */
class GameScoreQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return GameScore[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return GameScore|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
