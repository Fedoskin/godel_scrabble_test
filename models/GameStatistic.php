<?php

namespace app\models;

use app\models\query\GameStatisticQuery;
use Yii;

/**
 * This is the model class for table "{{%game_statistic}}".
 *
 * @property integer $game_id
 * @property integer $highScore
 * @property integer $lowScore
 * @property integer $avgScore
 * @property integer $winner_id
 * @property integer $loser_id
 */
class GameStatistic extends \yii\db\ActiveRecord
{
    public $game_name;

    public static function primaryKey()
    {
        return ['game_id', 'winner_id'];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_statistic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'highScore', 'lowScore', 'avgScore', 'winner_id', 'loser_id'], 'integer'],
            [['game_name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'game_id' => Yii::t('app', 'Game ID'),
            'highScore' => Yii::t('app', 'High Score'),
            'lowScore' => Yii::t('app', 'Low Score'),
            'winner_id' => Yii::t('app', 'Winner ID'),
            'loser_id' => Yii::t('app', 'Loser ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return GameStatisticQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameStatisticQuery(get_called_class());
    }

    public function getGame()
    {
        return $this->hasOne(Game::className(), ['id' => 'game_id']);
    }

    public function getWinner()
    {
        return $this->hasOne(User::className(), ['id' => 'winner_id']);
    }

    public function getLoser()
    {
        return $this->hasOne(User::className(), ['id' => 'loser_id']);
    }

    public static function getAverageScore($id)
    {
        return self::find()
            ->where(['OR', ['winner_id' => $id], ['loser_id' => $id]])
            ->average('avgScore');
    }

    public static function getHighestScore($id)
    {
        return self::find()
            ->where(['winner_id' => $id])
            ->orderBy('highScore desc')
            ->one();
    }
}
