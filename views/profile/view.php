<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'name',
            [
                'attribute' => 'wins',
                'label' => 'Wins',
                'value' => function($model) {
                    return $model->userStatistic->wins;
                }
            ],
            [
                'attribute' => 'loses',
                'label' => 'Loses',
                'value' => function($model) {
                    return $model->userStatistic->loses;
                }
            ],
            [
                'attribute' => 'average',
                'label' => 'Average score',
                'value' => round($model->userStatistic->avgScore, 2)
            ],
            [
                'attribute' => 'highestScore',
                'label' => 'Highest score, when, where, against whom',
                'value' => implode(', ', [
                    $highestScore->highScore,
                    $highestScore->game->game_date_at,
                    $highestScore->game->name,
                    $highestScore->loser->username
                ])
            ],
        ],
    ]) ?>

</div>
