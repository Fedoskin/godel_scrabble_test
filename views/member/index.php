<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            'email:email',
            [
                'attribute' => 'userStatistic.wins',
                'label' => 'Wins',
                'value' => function($model) {
                    return $model->userStatistic->wins;
                }
            ],
            [
                'attribute' => 'userStatistic.loses',
                'label' => 'Loses',
                'value' => function($model) {
                    return $model->userStatistic->loses;
                }
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Game Date',
                'value' => function($model) {
                    return date(Yii::$app->params['defaultDateFormat'], $model->created_at);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{profile} {view} {update} {delete}',
                'buttons' => [
                    'profile' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-user"></span>',
                            ['profile/view', 'id' => $model->id],
                            [
                                'title' => 'Profile',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
