<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email:email',
            //'password_hash',
            'auth_key',
            [
                'attribute' => 'loses',
                'value' => function($model) {
                    return $model->userStatistic->loses;
                }
            ],
            [
                'attribute' => 'confirmed_at',
                'value' => function($model) {
                    return date(Yii::$app->params['defaultDateFormat'], $model->confirmed_at);
                }
            ],
            'unconfirmed_email:email',
            [
                'attribute' => 'blocked_at',
                'value' => function($model) {
                    return $model->blocked_at ? date(Yii::$app->params['defaultDateFormat'], $model->blocked_at) : null;
                }
            ],
            'registration_ip',
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    return date(Yii::$app->params['defaultDateFormat'], $model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model) {
                    return $model->updated_at ? date(Yii::$app->params['defaultDateFormat'], $model->updated_at) : null;
                }
            ],
            [
                'attribute' => 'last_login_at',
                'value' => function($model) {
                    return date(Yii::$app->params['defaultDateFormat'], $model->last_login_at);
                }
            ],
        ],
    ]) ?>

</div>
