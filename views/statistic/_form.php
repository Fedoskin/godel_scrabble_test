<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GameStatistic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-statistic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'game_id')->textInput() ?>

    <?= $form->field($model, 'highScore')->textInput() ?>

    <?= $form->field($model, 'lowScore')->textInput() ?>

    <?= $form->field($model, 'avgScore')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'winner_id')->textInput() ?>

    <?= $form->field($model, 'loser_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
