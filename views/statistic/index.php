<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GameStatisticSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Game Statistics');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-statistic-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'game_name',
                'label' => 'Game Name',
                'value' => function($model) {
                    return $model->game->name;
                }
            ],
            'highScore',
            'lowScore',
            'avgScore',
            [
                'attribute' => 'winner_id',
                'label' => 'Winner Name',
                'value' => function($model) {
                    return $model->winner->username;
                }
            ],
            [
                'attribute' => 'loser_id',
                'label' => 'Loser Name',
                'value' => function($model) {
                    return $model->loser->username;
                }
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
