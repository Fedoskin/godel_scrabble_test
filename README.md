INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
composer global require "fxp/composer-asset-plugin:^1.3.1"
composer install

~~~

Now you should be able to access the application through the following URL

~~~
http://localhost/
~~~


CONFIGURATION
-------------

Run SQL script, which located in the root directory with name "sql_fake_data.sql"

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

### Project

When you will go to login page, pass credentials:
~~~
login: user@user.com
password: 111111
~~~